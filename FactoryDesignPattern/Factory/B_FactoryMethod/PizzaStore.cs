﻿using System.Collections.Generic;
using FactoryDesignPattern.Model;
using FactoryDesignPattern.Model.Pizza;

namespace FactoryDesignPattern.Factory
{
  public abstract class PizzaStore
  {
    public IPizza OrderPizza( IList<Ingiridient> ingridients )
    {
      IPizza pizza = CreatePizza( ingridients );
      pizza.Bake();
      pizza.Cut();
      pizza.Pack();
      return pizza;
    }
    public abstract IPizza CreatePizza( IList<Ingiridient> ingridients );
  }

  public class NewYorkPizzaStore : PizzaStore
  {
    public NewYorkPizzaStore()
    {
      System.Console.WriteLine( "Open New York Pizza Store" );
    }

    public override IPizza CreatePizza( IList<Ingiridient> ingridients )
    {
      System.Console.WriteLine( "Start making New York Pizza" );
      return new NewYorkPizza( ingridients );
    }
  }

  public class BostonPizzaStore : PizzaStore
  {
    public BostonPizzaStore()
    {
      System.Console.WriteLine( "Open Boston Pizza Store" );
    }

    public override IPizza CreatePizza( IList<Ingiridient> ingridients )
    {
      System.Console.WriteLine( "Start making Boston Pizza" );
      return new BostonPizza( ingridients );
    }
  }
}
