﻿using System.Collections.Generic;
using FactoryDesignPattern.Model;
using FactoryDesignPattern.Model.Pizza;

namespace FactoryDesignPattern.Factory
{
  public enum PizzaType
  {
    NewYork,
    Boston
  }

  public static class SimplePizzaFactory
  {
    public static IPizza CreatePizza( PizzaType type, IList<Ingiridient> ingiridients )
    {
      switch( type )
      {
        case PizzaType.NewYork:
          return new NewYorkPizza( ingiridients );
        case PizzaType.Boston:
          return new BostonPizza( ingiridients );
        default:
          return null;
      }
    }
  }
}
