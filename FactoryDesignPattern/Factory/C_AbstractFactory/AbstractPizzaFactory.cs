﻿using System.Collections.Generic;
using FactoryDesignPattern.Model;

namespace FactoryDesignPattern.Factory
{
  public abstract class ModernPizzaFactory
  {
    private readonly BasePizzaFactory _pizzaFactory;
    protected ModernPizzaFactory( BasePizzaFactory pizzaFactory )
    {
      _pizzaFactory = pizzaFactory;
    }

    public IPizza OrderPizza( IList<Ingiridient> ingiridients )
    {
      IPizza pizza = _pizzaFactory.CreatePizza( ingiridients );
      pizza.Bake();
      pizza.Cut();
      pizza.Pack();
      return pizza;
    }
  }

  public class NewYorkPizzaModernFactory : ModernPizzaFactory
  {
    public NewYorkPizzaModernFactory() : this( new NewYorkPizzaFactory() )
    { }

    public NewYorkPizzaModernFactory( BasePizzaFactory pizzaFactory ) : base( pizzaFactory )
    {
      System.Console.WriteLine( "Open New York Pizza Modern Factory" );
    }
  }

  public class BostonPizzaModernFactory : ModernPizzaFactory
  {
    public BostonPizzaModernFactory() : this(new BostonPizzaFactory())
    { }

    public BostonPizzaModernFactory( BasePizzaFactory pizzaFactory ) : base( pizzaFactory )
    {
      System.Console.WriteLine( "Open Boston Pizza Modern Factory" );
    }
  }
}
