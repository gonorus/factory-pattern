﻿using System.Collections.Generic;
using FactoryDesignPattern.Model;
using FactoryDesignPattern.Model.Pizza;

namespace FactoryDesignPattern.Factory
{
  public abstract class BasePizzaFactory
  {
    public abstract IPizza CreatePizza( IList<Ingiridient> ingiridients );
  }

  public class NewYorkPizzaFactory : BasePizzaFactory
  {
    public NewYorkPizzaFactory()
    {
      System.Console.WriteLine( "Open New York Pizza Factory" );
    }

    public override IPizza CreatePizza( IList<Ingiridient> ingiridients )
    {
      System.Console.WriteLine( "Start making New York Pizza" );
      return new NewYorkPizza( ingiridients );
    }
  }

  public class BostonPizzaFactory : BasePizzaFactory
  {
    public BostonPizzaFactory()
    {
      System.Console.WriteLine( "Open Boston Pizza Factory" );
    }

    public override IPizza CreatePizza( IList<Ingiridient> ingiridients )
    {
      System.Console.WriteLine( "Start making Boston Pizza" );
      return new BostonPizza( ingiridients );
    }
  }
}
