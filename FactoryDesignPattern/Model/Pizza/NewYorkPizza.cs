﻿using System.Collections.Generic;

namespace FactoryDesignPattern.Model.Pizza
{
  public class NewYorkPizza : BasePizza
  {
    public NewYorkPizza( IList<Ingiridient> ingridients ) : base( ingridients )
    {
      System.Console.WriteLine( "Ready to cook New York Pizza" );
    }

    public override void Bake()
    {
      System.Console.WriteLine( "Baking New York Pizza" );
    }

    public override void Cut()
    {
      System.Console.WriteLine( "Cuting New York Pizza" );
    }

    public override void Pack()
    {
      System.Console.WriteLine( "Packing New York Pizza" );
    }
  }
}
