﻿using System.Collections.Generic;

namespace FactoryDesignPattern.Model.Pizza
{
  public class BostonPizza : BasePizza
  {
    public BostonPizza( IList<Ingiridient> ingridients ) : base( ingridients )
    {
      System.Console.WriteLine( "Ready to cook Boston Pizza" );
    }

    public override void Bake()
    {
      System.Console.WriteLine( "Baking Boston Pizza" );
    }

    public override void Cut()
    {
      System.Console.WriteLine( "Cuting Boston Pizza" );
    }

    public override void Pack()
    {
      System.Console.WriteLine( "Packing Boston Pizza" );
    }
  }
}
