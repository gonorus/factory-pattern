﻿using System.Collections.Generic;

namespace FactoryDesignPattern.Model
{
  public enum Ingiridient
  {
    Sausage,
    Onion,
    Tomatoes,
    Mushroom,
    Tuna,
    Beacon
  }

  public enum Sauce
  {
    TomatoesSauce,
    ChilliSauce,
    ExtraHotSauce
  }

  public interface IPizza
  {
    IList<Ingiridient> Toppings { get; }
    Sauce SauceType { get; set; }
    void Bake();
    void Cut();
    void Pack();
  }

  public abstract class BasePizza : IPizza
  {
    protected BasePizza( IList<Ingiridient> ingridients )
    {
      Toppings = ingridients;
    }

    public IList<Ingiridient> Toppings { get; }
    public Sauce SauceType { get; set; }
    public abstract void Bake();
    public abstract void Cut();
    public abstract void Pack();
  }
}
