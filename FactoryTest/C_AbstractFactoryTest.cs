﻿using System.Collections.Generic;
using FactoryDesignPattern.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FactoryDesignPattern.Model.Pizza;
using FactoryDesignPattern.Factory;

namespace FactoryTest
{
  [TestClass]
  public class C_AbstractFactoryTest
  {
    [TestMethod]
    public void MakeNewYorkPizzaInNewYork()
    {
      var ingridients = new List<Ingiridient>() { Ingiridient.Beacon, Ingiridient.Onion, Ingiridient.Tomatoes };
      ModernPizzaFactory pizzaFactory = new NewYorkPizzaModernFactory();
      IPizza pizza = pizzaFactory.OrderPizza( ingridients );
      Assert.IsInstanceOfType( pizza, typeof( NewYorkPizza ) );
    }

    [TestMethod]
    public void MakeNewYorkPizzaInBoston()
    {
      var ingridients = new List<Ingiridient>() { Ingiridient.Beacon, Ingiridient.Onion, Ingiridient.Tomatoes };
      ModernPizzaFactory pizzaFactory = new BostonPizzaModernFactory( new NewYorkPizzaFactory() );
      IPizza pizza = pizzaFactory.OrderPizza( ingridients );
      Assert.IsInstanceOfType( pizza, typeof( NewYorkPizza ) );
    }

    [TestMethod]
    public void MakeBostonPizzaInNewYork()
    {
      var ingridients = new List<Ingiridient>() { Ingiridient.Beacon, Ingiridient.Onion, Ingiridient.Tomatoes };
      ModernPizzaFactory pizzaFactory = new NewYorkPizzaModernFactory( new BostonPizzaFactory() );
      IPizza pizza = pizzaFactory.OrderPizza( ingridients );
      Assert.IsInstanceOfType( pizza, typeof( BostonPizza ) );
    }

    [TestMethod]
    public void MakeBostonPizzaInBoston()
    {
      var ingridients = new List<Ingiridient>() { Ingiridient.Beacon, Ingiridient.Onion, Ingiridient.Tomatoes };
      ModernPizzaFactory pizzaFactory = new BostonPizzaModernFactory();
      IPizza pizza = pizzaFactory.OrderPizza( ingridients );
      Assert.IsInstanceOfType( pizza, typeof( BostonPizza ) );
    }
  }
}
