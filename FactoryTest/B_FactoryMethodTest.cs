﻿using System.Collections.Generic;
using FactoryDesignPattern.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FactoryDesignPattern.Model.Pizza;
using FactoryDesignPattern.Factory;

namespace FactoryTest
{
  [TestClass]
  public class B_FactoryMethodTest
  {
    [TestMethod]
    public void MakeNewYorkPizza()
    {
      var ingridients = new List<Ingiridient>() { Ingiridient.Beacon, Ingiridient.Onion, Ingiridient.Tomatoes };
      PizzaStore pizzaStore = new NewYorkPizzaStore();
      IPizza pizza = pizzaStore.OrderPizza( ingridients );
      Assert.IsInstanceOfType( pizza, typeof( NewYorkPizza ) );
    }

    [TestMethod]
    public void MakeBostonPizza()
    {
      var ingridients = new List<Ingiridient>() { Ingiridient.Beacon, Ingiridient.Onion, Ingiridient.Tomatoes };
      PizzaStore pizzaStore = new BostonPizzaStore();
      IPizza pizza = pizzaStore.OrderPizza( ingridients );
      Assert.IsInstanceOfType( pizza, typeof( BostonPizza ) );
    }
  }
}
