﻿using System.Collections.Generic;
using FactoryDesignPattern.Model;
using FactoryDesignPattern.Factory;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FactoryDesignPattern.Model.Pizza;

namespace FactoryTest
{
  [TestClass]
  public class A_SimpleFactoryTest
  {
    [TestMethod]
    public void MakeNewYorkPizza()
    {
      var ingridients = new List<Ingiridient>() { Ingiridient.Beacon, Ingiridient.Onion, Ingiridient.Tomatoes };
      IPizza pizza = SimplePizzaFactory.CreatePizza( PizzaType.NewYork, ingridients );
      Assert.IsInstanceOfType( pizza, typeof( NewYorkPizza ) );
    }

    [TestMethod]
    public void MakeBostonPizza()
    {
      var ingridients = new List<Ingiridient>() { Ingiridient.Beacon, Ingiridient.Onion, Ingiridient.Tomatoes };
      IPizza pizza = SimplePizzaFactory.CreatePizza( PizzaType.Boston, ingridients );
      Assert.IsInstanceOfType( pizza, typeof( BostonPizza ) );
    }
  }
}
